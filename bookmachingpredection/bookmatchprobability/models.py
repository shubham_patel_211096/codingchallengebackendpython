from django.db import models


# Create your models here.


class Book(models.Model):
    isbn = models.CharField(max_length=25)
    title = models.CharField(max_length=255)
    author_first_name = models.CharField(max_length=255)
    author_last_name = models.CharField(max_length=255)
    publishing_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title + " " + self.isbn
