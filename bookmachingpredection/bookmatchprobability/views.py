from django.shortcuts import render

# Create your views here.
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status


class CheckProbabilty(APIView):
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):
        result = {        }
        inputs = request.data
        if inputs.__len__() < 2:
            return Response({'detail':'Invalid Inputs/Books data is missing'}, status.HTTP_400_BAD_REQUEST)
        book_1_data = inputs[0]
        book_2_data = inputs[1]
        total_match_percent = 0
        if  book_1_data['isbn'] != '' and book_2_data['isbn'] != '' and book_1_data['isbn'] == book_2_data['isbn']:
            result['isbn'] = {'comment': 'ISBN matches: 100%', 'percent': '100%'}
            total_match_percent = 100
            content = {
                'result': result,
                'total_match_percent': total_match_percent,
                'user': str(request.user),
            }
            return Response(content)

        if book_1_data['title'] != '' and book_2_data['title'] != '' \
                and book_1_data['title'] == book_2_data['title']:
            result['title'] = {'comment': 'Title is the same', 'percent': '35%'}
            total_match_percent += 35
        if book_1_data['author_first_name'] != '' and book_2_data['author_first_name'] != '' \
                and book_1_data['author_first_name'] == book_2_data['author_first_name']:
            result['author_first_name'] = {'comment': 'Author first name match : +20%', 'percent': '20%'}
            total_match_percent += 20
        if book_2_data['author_last_name'] != '' and book_1_data['author_last_name'] != '' \
                and book_1_data['author_last_name'] == book_2_data['author_last_name']:
            result['author_last_name'] = {'comment': 'Author last name match : +25%', 'percent': '25%'}
            total_match_percent += 25
        if book_2_data['author_first_name'] != '' and book_1_data['author_first_name'] != ''\
                and (book_1_data['author_first_name'] in book_2_data['author_first_name'] \
                or book_2_data['author_first_name'] in book_1_data['author_first_name']) :
            result['author_first_name_similar'] = {'comment': 'Author first name similar : +15%', 'percent': '15%'}
            total_match_percent += 15
        if book_1_data['publishing_date'] != '' and book_2_data['publishing_date'] != '' \
                and book_1_data['publishing_date'] == book_2_data['publishing_date']:
            result['publishing_date'] = {'comment': 'Date matched', 'percent': '20%'}
            total_match_percent += 20
        content = {
            'result': result,
            'total_match_percent': total_match_percent,
            'user': str(request.user),
        }
        return Response(content)
