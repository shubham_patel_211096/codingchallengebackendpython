from django.apps import AppConfig


class BookmatchprobabilityConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bookmatchprobability'
