from django.urls import path, include
from .views import CheckProbabilty


urlpatterns = [
    path('checkprobability', CheckProbabilty.as_view()),
]
