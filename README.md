# DJANGO REST APPLICATION
install python
pip install django

cd to CodingChallengeBackendPython\bookmachingpredection
pip install -r requirements.txt

python manage.py makemigrations
python manage.py migrate

# FOR BASIC AUTH
python manage.py createsuperuser

python manage.py runserver 8000

API DOC : http://127.0.0.1:8000/api/v1/checkprobability
API Publish POSTMAN DOC URL : https://documenter.getpostman.com/view/11857810/UV5ZAbJi

